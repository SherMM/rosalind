import sys


def get_left_child(array, i):
    return 2 * i + 1


def get_right_child(array, i):
    return 2 * i + 2


def sift_down(array, i, size):
    idx = i
    l = get_left_child(array, i)
    if l < size and array[l] < array[idx]:
        idx = l
    r = get_right_child(array, i)
    if r < size and array[r] < array[idx]:
        idx = r
    if i != idx:
        swap(array, i, idx)
        sift_down(array, idx, size)


def build_min_heap(array):
    size = len(array)
    n = size // 2
    for i in range(n, -1, -1):
        sift_down(array, i, size)


def sort_k(array, k):
    build_min_heap(array)
    i, n = 0, len(array)-1
    s = []
    for _ in range(k):
        s.append(array[0])
        swap(array, 0, n)
        n -= 1
        sift_down(array, 0, n)
    return s


def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


if __name__ == "__main__":
    n, array, k = sys.stdin.read().splitlines()
    n, k = int(n), int(k)
    array = [int(val) for val in array.split()]
    s = sort_k(array, k)
    print(' '.join(str(val) for val in s))
