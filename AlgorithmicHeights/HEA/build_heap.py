import sys


def build_heap(array):
    n = len(array) // 2
    for i in range(n, -1, -1):
        sift_down(array, i)


def sift_down(array, i):
    max_idx = i
    size = len(array)
    l = get_left_child(i)
    if l < size and array[l] > array[max_idx]:
        max_idx = l
    r = get_right_child(i)
    if r < size and array[r] > array[max_idx]:
        max_idx = r
    if i != max_idx:
        swap(array, i, max_idx)
        sift_down(array, max_idx)


def get_right_child(i):
    return 2 * i + 2


def get_left_child(i):
    return 2 * i + 1

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


if __name__ == "__main__":
    n, *array = sys.stdin.read().splitlines()
    n = int(n)
    array = [int(val) for val in array[0].split()]
    build_heap(array)
    print(' '.join(str(val) for val in array))
