import sys


def insertion_sort(array):
    swaps = 0
    for i in range(1, len(array)):
        k = i
        while k > 0 and array[k] < array[k-1]:
            swap(array, k-1, k)
            swaps += 1
            k -= 1
    return swaps

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


if __name__ == "__main__":
    length, *array = sys.stdin.read().splitlines()
    length = int(length)
    array = [int(val) for val in array[0].split()]
    print(array)
    num_swaps = insertion_sort(array)
    print(array)
    print(num_swaps)
