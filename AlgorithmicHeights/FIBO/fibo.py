import sys

def fibo(n):
    p1, p2 = 0, 1
    for i in range(1, n):
        p1, p2 = p2, p1+p2
    return p2

if __name__ == "__main__":
    n = int(sys.argv[1])
    print(fibo(n))
