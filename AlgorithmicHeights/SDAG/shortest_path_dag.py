import sys
from collections import deque


class Edge:

    def __init__(self, u, w, weight=1):
        self.head = u
        self.tail = w
        self.weight = weight

    def __iter__(self):
        return iter([self.head, self.tail, self.weight])

    def __repr__(self):
        return 'Edge: ({} -> {}, w={})'.format(self.head, self.tail, self.weight)


class Vertex:

    def __init__(self, name):
        self.name = name
        self.adj = []
        self.d = float('inf')
        self.seen = False
        self.parent = None

    def __repr__(self):
        return 'Vertex({})'.format(self.name)


    def get_adj(self):
        return self.adj


class DiGraph:

    def __init__(self, v, e):
        self.num_verts = v
        self.num_edges = e
        self.verts = [None]
        self.edges = []
        for i in range(1, v+1):
            self.verts.append(Vertex(i))

    def __iter__(self):
        for i in range(1, len(self.verts)):
            yield self.verts[i]

    def __len__(self):
        return self.num_verts
        
    def get_num_verts(self):
        return self.num_verts

    def get_num_edges(self):
        return self.num_edges

    def get_vertex(self, i):
        return self.verts[i]

    def get_edges(self):
        for edge in self.edges:
            yield edge

    def add_edge(self, u, w, weight=1):
        head = self.verts[u]
        tail = self.verts[w]
        edge = Edge(head, tail, weight)
        head.adj.append(edge)
        self.edges.append(edge)

    def reset(self):
        for v in graph:
            v.d = float('inf')
            v.parent = None
            v.seen = False


def toposort(graph):
    graph.reset()
    order = deque()
    def visit(vertex):
        vertex.seen = True
        for _, node, _ in vertex.get_adj():
            if not node.seen:
                visit(node)
        order.appendleft(vertex)

    for vertex in graph:
        if not vertex.seen:
            visit(vertex)
    return order


def find_shortest_paths(graph, source):
    order = toposort(graph)
    graph.reset()
    source.d = 0
    for u in order:
        for _, v, weight in u.get_adj():
            if u.d != float('inf') and v.d > u.d + weight:
                v.d = u.d + weight
                v.parent = u



if __name__ == "__main__":
    v_e, *data = sys.stdin.read().splitlines()
    v, e = [int(val) for val in v_e.split()]
    graph = DiGraph(v, e)
    for edge in data:
        u, w, weight = [int(val) for val in edge.split()]
        graph.add_edge(u, w, weight)

    find_shortest_paths(graph, graph.get_vertex(1))
    for i in range(1, v+1):
        v = graph.get_vertex(i)
        if v.d == float('inf'):
            print('x', end=" ")
        else:
            print(v.d, end=" ")
    print()
