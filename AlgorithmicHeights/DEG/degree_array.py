import sys


def generate_degree_array(graph):
    array = []
    for i in range(1, len(graph)+1):
        array.append(len(graph[i]))
    return array


if __name__ == "__main__":
    v_e, *edges = sys.stdin.read().splitlines()
    v, e = [int(val) for val in v_e.split()]
    graph = dict()
    for i in range(1, v+1):
        graph[i] = []

    for edge in edges:
        u, w = [int(val) for val in edge.split()]
        graph[u].append(w)
        graph[w].append(u)

    print(' '.join(str(val) for val in generate_degree_array(graph)))
