import sys

def median(array, k):
    for i in range(k+1):
        min_idx = i
        min_val = array[i]
        for j in range(i+1, len(array)):
            if array[j] < min_val:
                min_idx = j
                min_val = array[j]
        swap(array, i, min_idx)
    return array[k-1]


def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


if __name__ == "__main__":
    n, array, k = sys.stdin.read().splitlines()
    n, k = int(n), int(k)
    array = [int(val) for val in array.split()]
    print(median(array, k))

