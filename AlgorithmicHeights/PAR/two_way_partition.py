import sys


def two_way_partition(array):
    piv = array[0]
    lo, hi = 0, len(array)-1
    swap(array, lo, hi)
    i = lo
    for j in range(lo, hi):
        if array[j] <= piv:
            swap(array, i, j)
            i += 1
    swap(array, i, hi)
    return i

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp



if __name__ == "__main__":
    n, *array = sys.stdin.read().splitlines()
    n = int(n)
    array = [int(val) for val in array[0].split()]
    two_way_partition(array)
    print(' '.join(str(val) for val in array))
