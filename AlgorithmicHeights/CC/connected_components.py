import sys
from collections import deque


def connected_components(graph):
    comps = 0
    seen = set()
    for i in range(1, len(graph)+1):
        if i not in seen:
            seen = seen.union(bfs(graph, i))
            comps += 1
    return comps

def bfs(graph, start):
    seen = set([start])
    queue = deque([start])
    while queue:
        curr = queue.popleft()
        for adj in graph[curr]:
            if adj not in seen:
                seen.add(adj)
                queue.append(adj)
    return seen


if __name__ == "__main__":
    v_e, *data = sys.stdin.read().splitlines()
    v, e = [int(val) for val in v_e.split()]
    
    graph = dict()

    for i in range(1, v+1):
        graph[i] = []
        
    for edge in data:
        u, v = [int(val) for val in edge.split()]
        graph[u].append(v)
        graph[v].append(u)

    print(connected_components(graph))
