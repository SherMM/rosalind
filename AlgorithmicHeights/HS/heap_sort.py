import sys

def build_max_heap(array):
    n = len(array) // 2
    for i in range(n, -1, -1):
        sift_down(array, i, len(array))


def sift_down(array, i, size):
    max_idx = i
    l = get_left_child(i)
    if l < size and array[l] > array[max_idx]:
        max_idx = l
    r = get_right_child(i)
    if r < size and array[r] > array[max_idx]:
        max_idx = r
    if i != max_idx:
        swap(array, i, max_idx)
        sift_down(array, max_idx, size)


def get_right_child(i):
    return 2 * i + 2


def get_left_child(i):
    return 2 * i + 1


def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


def heap_sort(array):
    size = len(array)-1
    build_max_heap(array)
    for _ in range(len(array)):
        swap(array, 0, size)
        size -= 1
        sift_down(array, 0, size)

if __name__ == "__main__":
    n, *array = sys.stdin.read().splitlines()
    n = int(n)
    array = [int(val) for val in array[0].split()]
    heap_sort(array)
    print(' '.join(str(val) for val in array))
