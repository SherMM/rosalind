import sys
from collections import defaultdict, deque

def bfs(graph):
    dists = dict()
    for i in range(1, len(graph)+1):
        dists[i] = -1
    dists[1] = 0
    queue = deque([1])
    while queue:
        curr = queue.popleft()
        for adj in graph[curr]:
            if dists[adj] == -1:
                dists[adj] = dists[curr] + 1
                queue.append(adj)
    return dists

    

if __name__ == "__main__":
    v_e, *data = sys.stdin.read().splitlines()
    v, e = [int(val) for val in v_e.split()]
    graph = dict()

    for i in range(1, v+1):
        graph[i] = []

    for edge in data:
        u, w = [int(val) for val in edge.split()]
        graph[u].append(w)
    
    dists = bfs(graph)
    for i in range(1, v+1):
        print(dists[i], end=" ")
    print()
