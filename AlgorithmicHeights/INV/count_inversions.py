import sys


def count_inversions(array):
    if len(array) <= 1:
        return 0, array
    mid = len(array) // 2
    cl, left = count_inversions(array[:mid])
    cr, right = count_inversions(array[mid:])
    c, arr = merge(left, right)
    return cl + cr + c, arr


def merge(arr1, arr2):
    count = 0
    array = []
    i, j = 0, 0
    a1 = len(arr1)
    while i < len(arr1) and j < len(arr2):
        if arr1[i] <= arr2[j]:
            array.append(arr1[i])
            i += 1
        else:
            count += len(arr1) - i
            array.append(arr2[j])
            j += 1
    if i == len(arr1):
        array.extend(arr2[j:])
    else:
        array.extend(arr1[i:])
    return count, array


if __name__ == "__main__":
    n, array = sys.stdin.read().splitlines()
    n = int(n)
    array = [int(val) for val in array.split()]
    count, _ = count_inversions(array)
    print(count)
    print(count_inversions([4, 2, 3, 5, 1]))
