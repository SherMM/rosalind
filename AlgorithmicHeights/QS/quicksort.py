import sys


def partition(array, lo, hi):
    piv = array[lo]
    i = lo
    swap(array, lo, hi)
    for j in range(lo, hi):
        if array[j] <= piv:
            swap(array, i, j)
            i += 1
    swap(array, i, hi)
    return i


def quicksort(array, lo, hi):
    if lo < hi:
        p = partition(array, lo, hi)
        quicksort(array, lo, p-1)
        quicksort(array, p+1, hi)
    

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


if __name__ == "__main__":
    n, *array = sys.stdin.read().splitlines()
    n = int(n)
    array = [int(val) for val in array[0].split()]
    quicksort(array, 0, len(array)-1)
    print(' '.join(str(val) for val in array))
