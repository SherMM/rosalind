import sys
import heapq as hp


def dijkstra(graph, source):
    dists = dict()
    for i in range(1, len(graph)+1):
        dists[i] = float("inf")
    dists[source] = 0

    pq = []
    for i in range(1, len(graph)+1):
        pq.append((dists[i], i))
    hp.heapify(pq)

    seen = set()
    while pq:
        d, v = hp.heappop(pq)
        if v not in seen:
            seen.add(v)
            for adj, w in graph[v]:
                if dists[adj] > d + w:
                    dists[adj] = d + w
                    hp.heappush(pq, (dists[adj], adj))
    for node in dists:
        if dists[node] == float("inf"):
            dists[node] = -1
    return dists
    

if __name__ == "__main__":
    v_e, *data = sys.stdin.read().splitlines()
    v, e = [int(val) for val in v_e.split()]
    graph = dict()
    
    for i in range(1, v+1):
        graph[i] = []

    for edge in data:
        u, w, wght = [int(val) for val in edge.split()]
        graph[u].append((w, wght))

    dij = dijkstra(graph, 1)
    for i in range(1, v+1):
        print(dij[i], end=" ")
    print()
