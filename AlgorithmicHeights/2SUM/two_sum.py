import sys
from collections import defaultdict


def two_sum(array):
    arr = sorted((v,i) for i,v in enumerate(array))
    sums = []
    for i in range(len(array)):
        if -array[i] == 0:
            continue
        idx = binary_search(arr, -array[i])
        if idx != -1:
            # index in original array
            jdx = arr[idx-1][1]
            if i < jdx:
                sums.append((i+1, jdx+1))
    if not sums:
        return -1
    return sums

def binary_search(array, value):
    start, end = 0, len(array)-1
    index = -1
    while start < end:
        mid = start + (end - start) // 2
        val = array[mid][0]
        if value < val:
            end = mid
        elif value > val:
            start = mid + 1
        else:
            index = mid + 1
            break
    return index


def two_sum_map(array):
    '''
    Takes an array A and returns the two
    indices, if any, if the integer values
    stored at those indices sum to 0
    '''
    two_sums = []
    idxs = defaultdict(list)
    for idx in range(len(array)):
        item = array[idx]
        if -item in idxs:
            two_sum = (idxs[-item][0], idx+1)
            two_sums.append(two_sum)
        idxs[item].append(idx+1)
    if len(two_sums) is 0:
        return -1
    return two_sums



if __name__ == "__main__":
    k_n, *arrays = sys.stdin.read().splitlines()
    k, n = [int(val) for val in k_n.split()]
    arrays = [[int(val) for val in arr.split()] for arr in arrays]
    
    '''
    for array in arrays:
        sums = two_sum(array)
        print(sums)
    print()
    '''
    
    for array in arrays:
        nums = two_sum_map(array)
        if nums == -1:
            print(nums)
        else:
            print(nums[0][0], nums[0][1])
