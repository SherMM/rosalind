import sys
from collections import defaultdict


def three_sum(array):
    matches = dict()
    for i in range(len(array)):
        matches[array[i]] = i
    for i in range(len(array)):
        for j in range(i+1, len(array)):
            item = -(array[i] + array[j])
            if item in matches:
                k = matches[item]
                if i < j < k:
                    return (i+1, j+1, k+1)
    return -1


if __name__ == "__main__":
    k_n, *arrays = sys.stdin.read().splitlines()
    k, n = [int(val) for val in k_n.split()]
    arrays = [[int(val) for val in arr.split()] for arr in arrays]
    for array in arrays:
        nums = three_sum(array)
        if nums == -1:
            print(nums)
        else:
            print(' '.join(str(val) for val in three_sum(array)))
