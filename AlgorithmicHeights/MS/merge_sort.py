import sys

def merge_sort(array):
    if len(array) <= 10:
        return insertion_sort(array)
    mid = (len(array) - 0) // 2
    left = merge_sort(array[:mid])
    right = merge_sort(array[mid:])
    return merge(left, right)

def merge(arr1, arr2):
    array = []
    i, j = 0, 0
    while i < len(arr1) and j < len(arr2):
        if arr1[i] <= arr2[j]:
            array.append(arr1[i])
            i += 1
        else:
            array.append(arr2[j])
            j += 1
    if i == len(arr1):
        array.extend(arr2[j:])
    else:
        array.extend(arr1[i:])
    return array

def insertion_sort(array):
    arr = array[:]
    for i in range(1, len(arr)):
        k = i
        while k > 0 and arr[k] < arr[k-1]:
            swap(arr, k-1, k)
            k -= 1
    return arr

def swap(array, i, j):
    temp = array[i]
    array[i] = array[j]
    array[j] = temp


if __name__ == "__main__":
    n, *array = sys.stdin.read().splitlines()
    n = int(n)
    array = [int(val) for val in array[0].split()]
    
    print(' '.join(str(val) for val in merge_sort(array)))
