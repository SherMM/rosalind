import sys


def merge(arr1, arr2):
    array = []
    i, j = 0, 0
    while i < len(arr1) and j < len(arr2):
        if arr1[i] <= arr2[j]:
            array.append(arr1[i])
            i += 1
        else:
            array.append(arr2[j])
            j += 1
    if i == len(arr1):
        array.extend(arr2[j:])
    else:
        array.extend(arr1[i:])
    return array


if __name__ == "__main__":
    n1, arr1, n2, arr2 = sys.stdin.read().splitlines()
    n1, n2 = int(n1), int(n2)
    arr1 = [int(val) for val in arr1.split()]
    arr2 = [int(val) for val in arr2.split()]

    print(' '.join(str(val) for val in merge(arr1, arr2)))
