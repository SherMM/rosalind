import sys

def binary_search(array, value):
    start, end = 0, len(array)-1
    index = -1
    while start < end:
        mid = start + (end - start) // 2
        val = array[mid]
        if value < val:
            end = mid
        elif value > val:
            start = mid + 1
        else:
            index = mid + 1
            break
    return index



if __name__ == "__main__":
    nstr, kstr, *arrays = sys.stdin.read().splitlines()
    n, k = int(nstr), int(kstr)
    array, queries = [[int(i) for i in arr.split()] for arr in arrays]
    for query in queries:
        print(binary_search(array, query))
