import sys


class Edge:

    def __init__(self, u, w, weight=1):
        self.head = u
        self.tail = w
        self.weight = weight

    def __iter__(self):
        return iter([self.head, self.tail, self.weight])

    def __repr__(self):
        return 'Edge: ({} -> {}, w={})'.format(self.head, self.tail, self.weight)


class Vertex:

    def __init__(self, name):
        self.name = name
        self.adj = []
        self.d = float('inf')
        self.seen = False
        self.parent = None

    def __repr__(self):
        return 'Vertex({})'.format(self.name)


    def get_adj(self):
        return self.adj


class DiGraph:

    def __init__(self, v, e):
        self.num_verts = v
        self.num_edges = e
        self.verts = [None]
        self.edges = []
        for i in range(1, v+1):
            self.verts.append(Vertex(i))

    def __iter__(self):
        for i in range(1, len(self.verts)):
            yield self.verts[i]

    def __len__(self):
        return self.num_verts
        
    def get_num_verts(self):
        return self.num_verts

    def get_num_edges(self):
        return self.num_edges

    def get_vertex(self, i):
        return self.verts[i]

    def get_edges(self):
        for edge in self.edges:
            yield edge

    def add_edge(self, u, w, weight=1):
        head = self.verts[u]
        tail = self.verts[w]
        edge = Edge(head, tail, weight)
        head.adj.append(edge)
        self.edges.append(edge)

def init(graph):
    for v in graph:
        v.d = float('inf')
        v.parent = None
        v.seen = False

def is_acyclic(graph):
    stack = set()

    def visit(vertex):
        if vertex.seen:
            return 1
        vertex.seen = True
        stack.add(vertex.name)
        for _, node, _ in vertex.get_adj():
            if node.name in stack or visit(node) == -1:
                return -1
        stack.remove(vertex.name)
        return 1

    for vertex in graph:
        if visit(vertex) == -1:
            return -1
    return 1



if __name__ == "__main__":
    n, *data = sys.stdin.read().splitlines()
    graphs = []
    idx = 0
    while idx < len(data):
        if not data[idx]:
            v, e = [int(val) for val in data[idx+1].split()]
            idx += 2
            graph = DiGraph(v, e)
            for i in range(idx, idx+e):
                u, w = [int(val) for val in data[i].split()]
                graph.add_edge(u, w)
            idx += e
            graphs.append(graph)   
    
    for graph in graphs:
        print(is_acyclic(graph), end=" ")
    print()
