import sys
from collections import Counter


def find_majority_element(array):
    c = Counter(array)
    n = len(array)
    largest = max(c, key=lambda x: c[x]/n)
    if c[largest] > n/2:
        return largest
    return -1



if __name__ == "__main__":
    k_n, *arrays = sys.stdin.read().splitlines()
    k, n = [int(val) for val in k_n.split()]
    arrays = [[int(val) for val in arr.split()] for arr in arrays]
    for array in arrays:
        print(find_majority_element(array), end=" ")
    print()
