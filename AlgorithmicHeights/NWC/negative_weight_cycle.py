import sys
from collections import deque

class Edge:

    def __init__(self, u, w, weight=1):
        self.head = u
        self.tail = w
        self.weight = weight

    def __getitem__(self):
        return self.head, self.tail, self.weight


    def __repr__(self):
        return 'Edge: ({}->{}, w={})'.format(self.head, self.tail, self.weight)


class Vertex:

    def __init__(self, name):
        self.name = name
        self.adj = []
        self.d = float('inf')
        self.seen = False
        self.parent = None
        self.count = 0


    def __repr__(self):
        return 'Vertex: {}'.format(self.name)


    def get_adj(self):
        return self.adj


class DiGraph:

    def __init__(self, v, e):
        self.num_verts = v
        self.num_edges = e
        self.verts = [None]
        self.edges = []
        for i in range(1, v+1):
            self.verts.append(Vertex(i))

    def __iter__(self):
        for i in range(1, len(self.verts)):
            yield self.verts[i]

    def __len__(self):
        return self.num_verts
        
    def get_num_verts(self):
        return self.num_verts

    def get_num_edges(self):
        return self.num_edges

    def get_vertex(self, i):
        return self.verts[i]

    def get_edges(self):
        for edge in self.edges:
            yield edge

    def add_edge(self, u, w, weight=1):
        head = self.verts[u]
        tail = self.verts[w]
        edge = Edge(head, tail, weight)
        head.adj.append(edge)
        self.edges.append(edge)

def init(graph):
    for v in graph:
        v.d = float('inf')
        v.parent = None
        v.count = 0
        v.seen = False


def bellman_ford(graph, source):
    init(graph)
    source.d = 0
    source.seen = True

    for i in range(1, len(graph)-1):
        for edge in graph.get_edges():
            h, t = edge.head, edge.tail
            if h.d + edge.weight < t.d:
                t.d = h.d + edge.weight
                t.parent = h

    for edge in graph.get_edges():
        h, t = edge.head, edge.tail
        if h.d + edge.weight < t.d:
            return 1
    return -1



if __name__ == "__main__":
    n, *data = sys.stdin.read().splitlines()
    graphs = []
    idx = 0
    while idx < len(data):
        g_data = data[idx].split()
        if len(g_data) == 2:
            v, e = [int(val) for val in g_data]
            v += 1
            idx += 1
            graph = DiGraph(v, e)
            for i in range(idx, idx+e):
                u, w, weight = [int(val) for val in data[i].split()]
                graph.add_edge(u, w, weight)
            # add dummy vertex connected to every edge with 0 weight
            dummy = graph.get_vertex(v)
            for i in range(1, len(graph)-1):
                v = graph.get_vertex(i)
                graph.add_edge(dummy.name, v.name, 0)
            idx += e
            graphs.append(graph)

    for graph in graphs:
        dummy = graph.get_vertex(len(graph))
        print(bellman_ford(graph, dummy), end=" ")
    print()
