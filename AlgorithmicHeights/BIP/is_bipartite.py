import sys
from collections import deque

class Edge:

    def __init__(self, u, w, weight=1):
        self.head = u
        self.tail = w
        self.weight = weight

    def __iter__(self):
        return iter([self.head, self.tail, self.weight])

    def __repr__(self):
        return 'Edge: ({} -> {}, w={})'.format(self.head, self.tail, self.weight)


class Vertex:

    def __init__(self, name):
        self.name = name
        self.adj = []
        self.d = float('inf')
        self.seen = False
        self.parent = None

    def __repr__(self):
        return 'Vertex({})'.format(self.name)


    def get_adj(self):
        return self.adj


class Graph:

    def __init__(self, v, e):
        self.num_verts = v
        self.num_edges = e
        self.verts = [None]
        self.edges = []
        for i in range(1, v+1):
            self.verts.append(Vertex(i))

    def __iter__(self):
        for i in range(1, len(self.verts)):
            yield self.verts[i]

    def __len__(self):
        return self.num_verts
        
    def get_num_verts(self):
        return self.num_verts

    def get_num_edges(self):
        return self.num_edges

    def get_vertex(self, i):
        return self.verts[i]

    def get_edges(self):
        for edge in self.edges:
            yield edge

    def add_edge(self, u, w, weight=1):
        head = self.verts[u]
        tail = self.verts[w]
        edge1 = Edge(head, tail, weight)
        edge2 = Edge(tail, head, weight)
        head.adj.append(edge1)
        tail.adj.append(edge2)
        self.edges.extend([edge1, edge2])

    def reset(self):
        for v in graph:
            v.d = float('inf')
            v.parent = None
            v.seen = False


def is_bipartite(graph):
    _, comps = get_connected_components(graph)
    def check_bipartite(graph, source):
        colors = []
        for i in range(len(graph)+1):
            colors.append(-1)
        colors[source.name] = 1
        
        queue = deque([source])
        while queue:
            u = queue.popleft()
            for _, v, _ in u.get_adj():
                if colors[v.name] == -1:
                    colors[v.name] = 1-colors[u.name]
                    queue.append(v)
                elif colors[v.name] == colors[u.name]:
                    return False
        return True
    
    # check that all components are bipartite
    results = []
    for comp in comps:
        src = comp[0]
        res = check_bipartite(graph, graph.get_vertex(src))
        if not res:
            return -1
    return 1

def get_connected_components(graph):
    count = 0
    comps = []

    def bfs(graph, source):
        source.seen = True
        queue = deque([source])
        comp = []
        while queue:
            curr = queue.popleft()
            comp.append(curr.name)
            for _, node, _ in curr.get_adj():
                if not node.seen:
                    node.seen = True
                    queue.append(node)
        comps.append(comp)
    
    for v in graph:
        if not v.seen:
            bfs(graph, v)
            count += 1
    return count, comps




if __name__ == "__main__":
    n, *data = sys.stdin.read().splitlines()
    graphs = []
    idx = 0
    while idx < len(data):
        if not data[idx]:
            v, e = [int(val) for val in data[idx+1].split()]
            idx += 2
            graph = Graph(v, e)
            for i in range(idx, idx+e):
                u, w = [int(val) for val in data[i].split()]
                graph.add_edge(u, w)
            idx += e
            graphs.append(graph)   
    
    for graph in graphs:
        print(is_bipartite(graph), end=" ")
    print()
