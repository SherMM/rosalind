import sys
from collections import deque

class Edge:

    def __init__(self, u, w, weight=1):
        self.head = u
        self.tail = w
        self.weight = weight


    def __repr__(self):
        return 'Edge: ({}->{}, w={})'.format(self.head, self.tail, self.weight)


class Vertex:

    def __init__(self, name):
        self.name = name
        self.adj = []
        self.d = float('inf')
        self.seen = False
        self.parent = None
        self.count = 0


    def __repr__(self):
        return 'Vertex: {}'.format(self.name)


    def get_adj(self):
        return self.adj


class DiGraph:

    def __init__(self, v, e):
        self.num_verts = v
        self.num_edges = e
        self.verts = [None]
        self.edges = []
        for i in range(1, v+1):
            self.verts.append(Vertex(i))

    def __iter__(self):
        for i in range(1, len(self.verts)):
            yield self.verts[i]
        
    def get_num_verts(self):
        return self.num_verts

    def get_num_edges(self):
        return self.num_edges

    def get_vertex(self, i):
        return self.verts[i]

    def get_edges(self):
        for edge in self.edges:
            yield edge

    def add_edge(self, u, w, weight=1):
        head = self.verts[u]
        tail = self.verts[w]
        edge = Edge(head, tail, weight)
        head.adj.append(edge)
        self.edges.append(edge)

def init(graph):
    for v in graph:
        v.d = float('inf')
        v.parent = None
        v.count = 0
        v.seen = False


def bellman_ford(graph, source):
    init(graph)
    source.d = 0
    source.seen = True

    queue = deque([source])
    while queue:
        u = queue.popleft()
        u.seen = False
        
        for edge in u.get_adj():
            v = edge.tail
            if (v.d > u.d + edge.weight):
                v.d = u.d + edge.weight
                v.parent = edge
                if not v.seen:
                    queue.append(v)
                    v.seen = True
    return get_distances(graph)

def get_distances(graph):
    dists = []
    for v in graph:
        if v.d == float('inf'):
            dists.append('x')
        else:
            dists.append(v.d)
    return dists


if __name__ == "__main__":
    v_e, *data = sys.stdin.read().splitlines()
    v, e = [int(val) for val in v_e.split()]
    edges = [tuple([int(val) for val in edge.split()]) for edge in data]
    graph = DiGraph(v, e)
    for (u, w, weight) in edges:
        graph.add_edge(u, w, weight)

    dists = bellman_ford(graph, graph.get_vertex(1))
    print(' '.join(str(val) for val in dists))
