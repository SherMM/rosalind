use std::io; 

fn fibo(n: i64, k: i64) -> i64 {
	let mut m2 = 1;
	let mut m1 = 0;
	let mut m2_old: i64;
	for _ in 0..n {
		m2_old = m2;
		m2 = m2 + k*m1;
		m1 = m2_old;

	}
	return m1;
}

fn main() {
	let mut numbers = String::new();
	io::stdin().read_line(&mut numbers).ok().expect("read error");

	let numbers: Vec<i64> = numbers
		      .split_whitespace()
		      .map(|s| s.parse().unwrap())
		      .collect();

    let n = numbers[0];
    let k = numbers[1];
    println!("{}", fibo(n, k));
}