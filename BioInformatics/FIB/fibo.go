package main 

import (
	"fmt"
	"os"
	"bufio"
	"strings"
	"strconv"
)

func Fibo(n int, k int) int {
	m2, m1 := 1, 0
	count := 0
	for count < n {
		m2, m1 = m2 + k*m1, m2
		count++
	}
	return m1
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	s, _ := reader.ReadString('\n')
	line := strings.Trim(s, "\n")
	nums := strings.Split(line, " ")
	nstr := nums[0]
	kstr := nums[1]
	n, _ := strconv.Atoi(nstr)
	k, _ := strconv.Atoi(kstr)
	fmt.Println(Fibo(n, k))
}