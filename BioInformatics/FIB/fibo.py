'''
Implements algorithm for calculating the fibonacci number
after n months with k pairs produced each month
'''
import sys

def fibo(n, k):
	'''
	Returns the n-th fibonacci number 
	where each rabbit produces k pairs
	'''
	m2, m1 = 1, 0
	for i in range(n):
		m2, m1 = m2 + k*m1, m2
	return m1


if __name__ == "__main__":
	n, k = [int(val) for val in sys.stdin.readline().strip().split()]
	print(fibo(n, k))