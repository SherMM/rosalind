'''
Transcribes a DNA string to an RNA string
'''
import sys

def transcribe_dna_to_rna(strand):
	'''
	Transcribes a DNA string to an RNA string. 
	Replaces T nucleotides with U
	'''
	rna = []
	for nuc in strand:
		if nuc == 'T':
			rna.append('U')
		else:
			rna.append(nuc)
	return ''.join(rna)


if __name__ == "__main__":
	strand = sys.stdin.readline().strip()
	print(transcribe_dna_to_rna(strand))