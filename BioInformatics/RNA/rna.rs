use std::io;

fn transcribe_dna_to_rna(strand: String) -> String {
	let mut rna = String::new();
	for c in strand.chars() {
		let nuc = c.to_string();
		if nuc == "T" {
			rna.push_str("U");
		} else {
			rna.push_str(&nuc);
		}

	}
	return rna;
}

fn main() {
	let mut strand = String::new();
	io::stdin().read_line(&mut strand).expect("Failed to read line");
	let strand : String = strand.trim().parse().expect("Error parsing string");
	let rna = transcribe_dna_to_rna(strand);
	println!("{}", rna);
}