package main 

import (
	"fmt"
	"os"
	"bytes"
	"bufio"
	"strings"
)

func TranscribeToRNA(strand string) string {
	var rna bytes.Buffer
	for _, letter := range strand {
		nuc := string(letter)
		if nuc == "T" {
			rna.WriteString("U")
		} else {
			rna.WriteString(nuc)
		}
	}
	return rna.String()
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	s, _ := reader.ReadString('\n')
	strand := strings.Trim(s, "\n")
	rna := TranscribeToRNA(strand)
	fmt.Println(rna)
}