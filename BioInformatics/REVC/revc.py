'''
Generates the reverse complement of a DNA strand
'''
import sys

def reverse_comp(strand):
	'''
	Returns the reverse complement of a DNA
	strand. A->T, T->A, G->C, C->G
	'''
	mappings = {
		'A': 'T', 'T': 'A', 
		'G': 'C', 'C': 'G'
	}
	revc = []
	for i in range(len(strand)-1, -1, -1):	
		revc.append(mappings[strand[i]])
	return ''.join(revc)


if __name__ == "__main__":
	strand = sys.stdin.readline().strip()
	print(reverse_comp(strand))