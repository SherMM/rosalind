package main 

import (
	"fmt"
	"os"
	"bytes"
	"bufio"
	"strings"
)

func ReverseComplement(strand string) string {
	var revc bytes.Buffer
	for i := len(strand)-1; i >= 0; i-- {
		switch string(strand[i]) {
			case "A":
				revc.WriteString("T")
			case "T":
				revc.WriteString("A")
			case "C":
				revc.WriteString("G")
			case "G":
				revc.WriteString("C")
			default:
				fmt.Println("Unknown Char")
		}
	}
	return revc.String()
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	s, _ := reader.ReadString('\n')
	strand := strings.Trim(s, "\n")
	revc := ReverseComplement(strand)
	fmt.Println(revc)
}