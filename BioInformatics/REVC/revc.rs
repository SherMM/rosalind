use std::io;

fn reverse_comp(strand: String) -> String {
	let mut revc = String::new();
	for c in strand.chars().rev() {
		match c {
			'A' => revc.push_str("T"),
			'T' => revc.push_str("A"),
			'C' => revc.push_str("G"),
			'G' => revc.push_str("C"),
			 _  => println!("Unknown char"),
		}
	}
	return revc;
}

fn main() {
	let mut strand = String::new();
	io::stdin().read_line(&mut strand).expect("Failed to read line");
	let strand : String = strand.trim().parse().expect("Error parsing string");
	let revc = reverse_comp(strand);
	println!("{}", revc);
}