use std::io;
use std::io::prelude::*;
//use std::collections::HashMap;

fn main() {
	//let mut fasta = HashMap::new();
	//let mut curr_fid = String::new();
	let stdin = io::stdin();
	for line in stdin.lock().lines() {
		let curr_line = line.unwrap();
		if curr_line.starts_with(">") {
			println!("{}", curr_line);
		}
	}
}