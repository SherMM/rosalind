'''
Implements algorithm to find the GC content of a 
DNA string, which is the percentage of symbols in the string
that are G or C
'''
import sys
from collections import Counter

def gc_content(strand):
	'''
	Returns GC percentage for a strand of
	DNA, which is percentage of nucleotides
	that are either G or C
	'''
	c = Counter(strand)
	return (c['G'] + c['C']) / sum(c.values())

def get_largest_gc_content(strands):
	'''
	Returns fasta ID and gc-content percentage of
	DNA string with highest gc-content percentage
	'''
	best = ""
	best_val = 0.0
	for fid, strand in strands.items():
		gc = gc_content(strand)
		if gc > best_val:
			best, best_val = fid, gc
	return best, best_val
	

if __name__ == "__main__":
	fasta = dict()
	# get current fasta id we are building strand for
	curr_fid = ""
	for line in sys.stdin.readlines():
		if line.startswith(">"):
			fid = line[1:].strip()
			fasta[fid] = ""
			curr_fid = fid 
		else:
			fasta[curr_fid] += line.strip()
	
	best_gc_id, best_gc_val = get_largest_gc_content(fasta)
	print(best_gc_id)
	print(best_gc_val*100)