package main 

import (
	"fmt"
	"os"
	"bufio"
	"strings"
)

func CountNucleotides(strand string) ([4]int) {
	counts := [4]int{0, 0, 0, 0}
	for _, letter := range strand {
		nuc := string(letter)
		switch nuc {
			case "A":
				counts[0] += 1
			case "C":
				counts[1] += 1
			case "G":
				counts[2] += 1
			case "T":
				counts[3] += 1
			default:
				fmt.Println("Unknown Char")
		}
	}
	return counts
}

func main() {
	reader := bufio.NewReader(os.Stdin)
	s, _ := reader.ReadString('\n')
	strand := strings.Trim(s, "\n")
	counts := CountNucleotides(strand)
	for i := 0; i < len(counts); i++ {
		fmt.Print(counts[i])
		fmt.Print(" ")
	}
	fmt.Println("")
}