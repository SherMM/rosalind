use std::io;

fn count_nucleotides(strand: String) -> [i32; 4] {
	let mut counts: [i32; 4] = [0, 0, 0, 0];
	for c in strand.chars() {
		match c {
			'A' => counts[0] += 1,
			'C' => counts[1] += 1,
			'G' => counts[2] += 1,
			'T' => counts[3] += 1,
			 _  => println!("Unknown char"),
		}
	}
	return counts;
}

fn main() {
	let mut strand = String::new();
	io::stdin().read_line(&mut strand).expect("Failed to read line");
	let strand : String = strand.trim().parse().expect("Error parsing string");
	let counts = count_nucleotides(strand);
	for count in &counts {
		print!("{}", count);
		print!(" ");
	}
	println!("");
}


