import sys
import array
import time
from collections import Counter

def count_nucleotides(strand):
	'''
	Counts the number of A, C, G, T nucleotides
	in a DNA string. Returns arrays of coutns 
	'''
	counts = array.array('i', (0 for i in range(4)))
	for nuc in strand:
		if nuc == 'A':
			counts[0] += 1
		elif nuc == 'C':
			counts[1] += 1
		elif nuc == 'G':
			counts[2] += 1
		else:
			counts[3] += 1
	return counts

def counter_nucleotides(strand):
	'''
	Counts the number of A, C, G, T nucleotides
	in a DNA string. Returns arrays of coutns 
	'''
	counts = Counter(strand)
	return counts


if __name__ == "__main__":
	strand = sys.stdin.readlines()[0].strip()
	start = time.time()
	counts = count_nucleotides(strand)
	end = time.time()
	print(' '.join(str(count) for count in counts))
	print("Runtime: {}".format(end-start))

	start = time.time()
	counter = counter_nucleotides(strand)
	end = time.time()
	print(' '.join(str(counter[nuc]) for nuc in counter))
	print("Runtime: {}".format(end-start))